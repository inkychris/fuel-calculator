# fuel-calculator
[A simple app](https://inkychris.gitlab.io/fuel-calculator)
for predicting fuel requirements for sim racing.

## Features
- Mobile and "using a mouse with gloves on" friendly
- Keyboard input also supported
- Predicted lap count is rounded up to nearest whole number
- 100% front-end - runs entirely in your browser
